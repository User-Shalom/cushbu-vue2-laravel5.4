# README #

### What is this repository for? ###

A Laravel5.4 Vue2 admin panel for cushbu.com admin panel

### How do I get set up? ###

* Laravel 5.4
* ACL for laravel 5.4
* Module structure for Laravel 5.4
* AdminLTE basic integration
* Vuejs2
* Axios
* Vue-table-2
